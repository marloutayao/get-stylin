-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 22, 2020 at 05:12 PM
-- Server version: 10.4.10-MariaDB
-- PHP Version: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `get_stylin`
--

-- --------------------------------------------------------

--
-- Table structure for table `accounts`
--

CREATE TABLE `accounts` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `accounts`
--

INSERT INTO `accounts` (`id`, `username`, `password`) VALUES
(1, 'marlou', 'tayao');

-- --------------------------------------------------------

--
-- Table structure for table `landing_feature`
--

CREATE TABLE `landing_feature` (
  `id` int(11) NOT NULL,
  `feature_header` varchar(255) NOT NULL,
  `feature_message` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `landing_feature`
--

INSERT INTO `landing_feature` (`id`, `feature_header`, `feature_message`) VALUES
(1, 'Be Chic, Be Bold, Be You', 'Features. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat aliquip ex ea ');

-- --------------------------------------------------------

--
-- Table structure for table `landing_feature_image`
--

CREATE TABLE `landing_feature_image` (
  `id` int(11) NOT NULL,
  `img_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `landing_feature_image`
--

INSERT INTO `landing_feature_image` (`id`, `img_name`) VALUES
(1, 'getstylin-banner-img.png'),
(2, 'getstylin-myrewards.png'),
(3, 'gs-mockup.png'),
(4, 'getstylin-banner-img.png');

-- --------------------------------------------------------

--
-- Table structure for table `landing_footer`
--

CREATE TABLE `landing_footer` (
  `id` int(11) NOT NULL,
  `footer_message` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `landing_footer`
--

INSERT INTO `landing_footer` (`id`, `footer_message`) VALUES
(1, 'Walk in for the ');

-- --------------------------------------------------------

--
-- Table structure for table `landing_highlights`
--

CREATE TABLE `landing_highlights` (
  `id` int(11) NOT NULL,
  `highlight_message` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `landing_highlights`
--

INSERT INTO `landing_highlights` (`id`, `highlight_message`) VALUES
(14, 'Highlights. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor. '),
(15, 'Highlights. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor. '),
(16, 'Highlights. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor. ');

-- --------------------------------------------------------

--
-- Table structure for table `landing_intro`
--

CREATE TABLE `landing_intro` (
  `id` int(11) NOT NULL,
  `intro_header` varchar(255) NOT NULL,
  `intro_message` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `landing_intro`
--

INSERT INTO `landing_intro` (`id`, `intro_header`, `intro_message`) VALUES
(1, 'SHOW OFF YOUR BEST OUTFIT!', 'Lorem Ipsum sit dolor amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.');

-- --------------------------------------------------------

--
-- Table structure for table `landing_logo`
--

CREATE TABLE `landing_logo` (
  `id` int(11) NOT NULL,
  `img_name` varchar(255) NOT NULL,
  `device` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `landing_logo`
--

INSERT INTO `landing_logo` (`id`, `img_name`, `device`) VALUES
(1, 'gs-logo-2.png', 'mobile'),
(2, 'getstylin-logo.png', 'pc');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accounts`
--
ALTER TABLE `accounts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `landing_feature`
--
ALTER TABLE `landing_feature`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `landing_feature_image`
--
ALTER TABLE `landing_feature_image`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `landing_footer`
--
ALTER TABLE `landing_footer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `landing_highlights`
--
ALTER TABLE `landing_highlights`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `landing_intro`
--
ALTER TABLE `landing_intro`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `landing_logo`
--
ALTER TABLE `landing_logo`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accounts`
--
ALTER TABLE `accounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `landing_feature`
--
ALTER TABLE `landing_feature`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `landing_feature_image`
--
ALTER TABLE `landing_feature_image`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `landing_footer`
--
ALTER TABLE `landing_footer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `landing_highlights`
--
ALTER TABLE `landing_highlights`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `landing_intro`
--
ALTER TABLE `landing_intro`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `landing_logo`
--
ALTER TABLE `landing_logo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
