<?php
include("./controllers/connection.php");

$get_pc_logo = "SELECT `id`, `img_name`, `device` FROM `landing_logo` WHERE `device` = 'pc'";
$result = mysqli_query($conn, $get_pc_logo);
$pc_logo = mysqli_fetch_assoc($result);

$get_mobile_logo = "SELECT `id`, `img_name`, `device` FROM `landing_logo` WHERE `device` = 'mobile'";
$result = mysqli_query($conn, $get_mobile_logo);
$mobile_logo = mysqli_fetch_assoc($result);


$get_intro = "SELECT * FROM `landing_intro`";
$result = mysqli_query($conn, $get_intro);
$intro_details = mysqli_fetch_assoc($result);


$get_highlights = "SELECT * FROM `landing_highlights`";
$highlights = mysqli_query($conn, $get_highlights);

$get_feature = "SELECT * FROM `landing_feature`";
$result = mysqli_query($conn, $get_feature);
$feature_details = mysqli_fetch_assoc($result);

$get_first_picture = "SELECT * FROM `landing_feature_image` WHERE `id` = 1";
$result = mysqli_query($conn, $get_first_picture);
$first_picture = mysqli_fetch_assoc($result);

$get_second_picture = "SELECT * FROM `landing_feature_image` WHERE `id` = 2";
$result = mysqli_query($conn, $get_second_picture);
$second_picture = mysqli_fetch_assoc($result);

$get_third_picture = "SELECT * FROM `landing_feature_image` WHERE `id` = 3";
$result = mysqli_query($conn, $get_third_picture);
$third_picture = mysqli_fetch_assoc($result);

$get_fourth_picture = "SELECT * FROM `landing_feature_image` WHERE `id` = 4";
$result = mysqli_query($conn, $get_fourth_picture);
$fourth_picture = mysqli_fetch_assoc($result);

$get_footer_message = "SELECT * FROM `landing_footer`";
$result = mysqli_query($conn, $get_footer_message);
$landing_footer_details = mysqli_fetch_assoc($result);

?>
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Get Stylin</title>

  <!-- Bootstrap core CSS -->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="css/getstylin-custom.css" rel="stylesheet">
  <link href='http://fonts.googleapis.com/css?family=Raleway:100,400,600,700' rel='stylesheet' type='text/css'>
  <link href='http://fonts.googleapis.com/css?family=Roboto:100,400,600,700' rel='stylesheet' type='text/css'>

</head>

<body>
  <!-- Page Content -->
  <section class="row no-gutters">
    <div class="col-md-6 col-lg-5 container gs-intro d-flex flex-column">
      <div class="mx-3 mx-md-4 mx-lr-5">
        <div class="gs-logo mobile mt-5 pb-5 hide-medium">
          <img class="img-fluid" src="assets/<?= $mobile_logo["img_name"]  ?>" alt="Get Stylin Logo">
        </div>
        <div class="gs-logo mt-5 pb-5 hide-small">
          <a href="/views/login.php"> <img class="img-fluid" src="assets/<?= $pc_logo["img_name"]  ?>" alt="Get Stylin Logo"> </a>
        </div>
        <div class="gs-intro-text mt-md-5">
          <h1><?= $intro_details["intro_header"] ?></h1>
          <p class="mt-2"><?= $intro_details["intro_message"] ?></p>
        </div>
        <div class="row my-5 hide-medium">
          <div class="gs-mobile-mockup"><img class="img-fluid" src="assets/gs-mockup.png" alt="Get Stylin App"></div>
        </div>
        <div class="row mb-5 mt-4 mt-md-4">
          <div class="col d-flex">
            <div class="btn-google-play btn-download col-md-auto px-1 pr-sm-3"><img src="assets/getstylin-google-play-btn.png" alt="Get Stylin GET IT ON GOOGLE PLAY">
            </div>

            <div class="btn-app-store btn-download col-md-auto px-1"><img src="assets/getstylin-app-store-btn.png" alt="Get Stylin Download on the App Store">
            </div>
          </div>
        </div>
      </div>

      <div class="align-items-end mt-auto">
        <div class="col-md-12 p-3 p-md-5 gs-highlights">
          <ul>
            <?php
            foreach ($highlights as $highlight) {
            ?>
              <li class="pt-3">
                <?= $highlight["highlight_message"] ?>
              </li>
            <?php
            }
            ?>
          </ul>
        </div>
      </div>
    </div>
    <div class="col-md-6 col-lg-7 gs-banner">
      <div class="gs-mockup hide-small"><img class="img-fluid" src="assets/gs-mockup.png" alt="Get Stylin App"></div>
      <img class="img-fluid" src="assets/getstylin-banner-img.png" alt="Get Stylin Banner Image">
    </div>
  </section>

  <section class="gs-section features  py-5">
    <div class="container">
      <div class="row">
        <div class="col align-self-center text-center pb-3 pb-md-5 mx-3 mx-md-5">
          <h3><?= $feature_details["feature_header"] ?></h3>
          <p><?= $feature_details["feature_message"] ?></p>
        </div>

      </div>


      <div id="carouselExampleIndicators" class="carousel slide hide-medium" data-ride="carousel">
        <ol class="carousel-indicators">
          <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
        </ol>
        <div class="carousel-inner">
          <div class="carousel-item active">
            <img src="assets/<?= $first_picture["img_name"] ?>" class="d-block w-100" alt="...">
          </div>
          <div class="carousel-item">
            <img src="assets/<?= $second_picture["img_name"] ?>" class="d-block w-100" alt="...">
          </div>
          <div class="carousel-item">
            <img src="assets/<?= $third_picture["img_name"] ?>" class="d-block w-100" alt="...">
          </div>
          <div class="carousel-item">
            <img src="assets/<?= $fourth_picture["img_name"] ?>" class="d-block w-100" alt="...">
          </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>


      <div class="row hide-small">
        <div class="col-md-3"><img class="img-fluid" src="assets/<?= $first_picture["img_name"] ?>" alt="Get Stylin"></div>
        <div class="col-md-3"><img class="img-fluid" src="assets/<?= $second_picture["img_name"] ?>" alt="Get Stylin"></div>
        <div class="col-md-3"><img class="img-fluid" src="assets/<?= $third_picture["img_name"] ?>" alt="Get Stylin"></div>
        <div class="col-md-3"><img class="img-fluid" src="assets/<?= $fourth_picture["img_name"] ?>" alt="Get Stylin"></div>
      </div>

    </div>

  </section>
  <section class="gs-section contact">
    <div class="container pt-5">
      <div class="row">
        <div class="col align-self-center text-center pb-5 mx-5">
          <h3>Contact Us</h3>
          <p>Get in touch with us to get the latest trends!</p>
        </div>
      </div>

      <div class="d-flex flex-wrap justify-content-center">
        <div class="gs-social px-md-3">
          <a href="mailto:info@getstylin.com">
            <img src="assets/gs-mail-icon.svg" alt="Get Stylin Email">
            <div class="card-body">
              <h5>Email</h5>
              <p>info@getstylin.com</p>
            </div>
          </a>
        </div>

        <div class="gs-social px-md-3 ">
          <a href="www.facebook.com/getstylin">
            <img src="assets/gs-facebook-icon.svg" alt="Get Stylin Facebook">
            <div class="card-body text-center">
              <h5>Facebook</h5>
              <p>Get Stylin</p>
            </div>
          </a>
        </div>

        <div class="gs-social px-md-3">
          <a href="www.twitter.com/getstylin">
            <img src="assets/gs-twitter-icon.svg" alt="Get Stylin Twitter">
            <div class="card-body">
              <h5>Twitter</h5>
              <p>@getstylin</p>
            </div>
          </a>
        </div>

        <div class="gs-social px-md-3">
          <a href="www.instagram.com/getstylin">
            <img src="assets/gs-instagram-icon.svg" alt="Get Stylin Instagram">
            <div class="card-body">
              <h5>Instagram</h5>
              <p>@getstylin</p>
            </div>
          </a>
        </div>
      </div>
    </div>
  </section>

  <section class="gs-section footer">
    <div class="container pt-5">
      <div class="row">
        <div class="col align-self-center text-center pb-5 mx-md-5 mx-md-3">
          <h3><?= $landing_footer_details["footer_message"] ?></h3>
          <div class="col mt-5 d-flex justify-content-center">
            <div class="btn-google-play px-1 btn-download col-md-auto"><img src="assets/getstylin-google-play-btn.png" alt="Get Stylin GET IT ON GOOGLE PLAY">
            </div>

            <div class="btn-app-store px-1 btn-download col-md-auto"><img src="assets/getstylin-app-store-btn.png" alt="Get Stylin Download on the App Store">
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>


  <!-- Bootstrap core JavaScript -->
  <script src="vendor/jquery/jquery.slim.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script type="text/javascript">
    $('.carousel').carousel({
      interval: 2000,
      keyboard: true,
      touch: true
    })
  </script>

</body>

</html>