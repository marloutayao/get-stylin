<?php
include("connection.php");
session_start();
if (isset($_SESSION["user"])) {

    $img_id = $_POST['img_id'];
    $imgName = $_FILES["img"]["name"];
    $imgSize = $_FILES["img"]["size"];
    $imgTmpName = $_FILES["img"]["tmp_name"];
    $imgType = strtolower(pathinfo($imgName, PATHINFO_EXTENSION));

    $isImage = false;
    $maxsize = 8388608;
    $hasDetails = false;

    if ($imgType == "jpg" || $imgType == "jpeg" || $imgType == "png") {
        $image = "../assets/$imgName";
        move_uploaded_file($imgTmpName, $image);

        $update_picture = "UPDATE `landing_feature_image` SET `img_name` = '$imgName' WHERE `id` = '$img_id'";

        mysqli_query($conn, $update_picture);
        $isImage = true;
    }

    if ($isImage) {
        $_SESSION["third_image_updated"] = true;
        header("Location: ../views/feature.php");
    } else {
        $_SESSION["third_image_updated"] = false;
        header("Location: ../views/feature.php");
    }
} else {
    header("Location: ../views/login.php");
}
