<?php

$host = "localhost"; // host to use
$username = "root"; // user for the host
$password = ""; // password
$db = "get_stylin"; // db name

// open connection

$conn = mysqli_connect($host, $username, $password, $db);

if(!$conn) {
	die("Can't connect to the Database..." . mysqli_error($conn));
}
