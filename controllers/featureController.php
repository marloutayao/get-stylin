<?php
session_start();
include("connection.php");
if (isset($_SESSION["user"])) {

    $feature_header = mysqli_real_escape_string($conn, $_POST["feature_header"]);
    $feature_message = mysqli_real_escape_string($conn, $_POST["feature_message"]);
    $feature_id =  mysqli_real_escape_string($conn, $_POST["feature_id"]);

    // print_r($_POST);
    $isAdded = false;

    if ($feature_header == "" || $feature_message == "") {
        $isAdded = false;
    } else {
        $save_feature = "UPDATE `landing_feature` SET `feature_header` = '$feature_header', `feature_message` = '$feature_message' WHERE `id` = '$feature_id'";
        mysqli_query($conn, $save_feature);
        $isAdded = true;
    }




    if ($isAdded) {
        $_SESSION["feature_updated"] = true;
        header("Location: ../views/feature.php");
    } else {
        $_SESSION["feature_updated"] = false;
        header("Location: ../views/feature.php");
    }
} else {
    header("Location: ../views/login.php");
}
