<?php
include("connection.php");

session_start();
$username = mysqli_real_escape_string($conn, $_POST["username"]);
$password = mysqli_real_escape_string($conn, $_POST["password"]);

$accounts_query = "SELECT * FROM accounts WHERE username = '$username' ";
$result = mysqli_query($conn, $accounts_query);

$user_info = mysqli_fetch_assoc($result);
$error = true;

if (mysqli_num_rows($result) > 0) {
    if ($user_info["password"] != $password) {
        die("Login failed");
    } else {
        $_SESSION["user"] = $user_info;
        $error = false;
    }
}

if ($error) {
    $_SESSION["error_login"] = true;
    header("Location: ../views/login.php");
} else {
    header("Location: ../views/home.php");
}
