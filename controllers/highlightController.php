<?php
session_start();
include("connection.php");
if (isset($_SESSION["user"])) {

    $method = $_POST["highlight_method"];
    $isDeleted = false;
    $isMessage = false;

    if ($method == "remove_highlight") {
        $highlight_id = $_POST["highlight_id"];

        $delete_highlight = "DELETE FROM `landing_highlights` WHERE `id` = $highlight_id";
        mysqli_query($conn, $delete_highlight);
        $isDeleted = true;
    }

    if ($method == "add_highlight") {
        $highlight_message = mysqli_real_escape_string($conn, $_POST["highlight_message"]);

        if ($highlight_message == "") {
            $isMessage = false;
            $_SESSION["highlight_added"] = false;
            header("Location: ../views/highlight.php");
        } else {
            $add_highlight = "INSERT INTO `landing_highlights` (`highlight_message`) VALUES ('$highlight_message')";

            mysqli_query($conn, $add_highlight);
            $isMessage = true;
        }
    }
} else {
    header("Location: ../views/login.php");
}



if ($isDeleted) {
    $_SESSION["highlight_deleted"] = true;
    header("Location: ../views/highlight.php");
}

if ($isMessage) {
    $_SESSION["highlight_added"] = true;
    header("Location: ../views/highlight.php");
}
