<?php
session_start();
include("connection.php");
if (isset($_SESSION["user"])) {

    $landing_footer_message = mysqli_real_escape_string($conn, $_POST["landing_footer_message"]);
    $isSuccess = false;

    if ($landing_footer_message == "") {
        $isSuccess = false;
    } else {
        $update_footer = "UPDATE `landing_footer` SET `footer_message` = '$landing_footer_message' WHERE `id` = 1";
        mysqli_query($conn, $update_footer);
        $isSuccess = true;
    }


    if ($isSuccess) {
        $_SESSION["footer_updated"] = true;
        header("Location: ../views/landing_footer.php");
    } else {
        $_SESSION["footer_updated"] = false;
        header("Location: ../views/landing_footer.php");
    }
} else {
    header("Location: ../views/login.php");
}
