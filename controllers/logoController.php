<?php
include("connection.php");
session_start();
if (isset($_SESSION["user"])) {

    $logoImgName = $_FILES["logoImg"]["name"];
    $logoImgSize = $_FILES["logoImg"]["size"];
    $logoImgTmpName = $_FILES["logoImg"]["tmp_name"];
    $logoImgType = strtolower(pathinfo($logoImgName, PATHINFO_EXTENSION));

    $isImage = false;
    $maxsize = 8388608;
    $hasDetails = false;

    // echo $_POST["change_options"];
    if ($logoImgType == "jpg" || $logoImgType == "jpeg" || $logoImgType == "png") {
        $logoImage = "../assets/$logoImgName";
        move_uploaded_file($logoImgTmpName, $logoImage);

        if ($_POST["change_options"] === "pc_only") {
            $update_pc_logo = "UPDATE `landing_logo` SET `img_name`= '$logoImgName' WHERE `device` = 'pc'";
            mysqli_query($conn, $update_pc_logo);
        } else if ($_POST["change_options"] === "mobile_only") {
            $update_mobile_logo = "UPDATE `landing_logo` SET `img_name`= '$logoImgName' WHERE `device` = 'mobile'";
            mysqli_query($conn, $update_mobile_logo);
        } else {
            $update_pc_logo = "UPDATE `landing_logo` SET `img_name`= '$logoImgName' WHERE `device` = 'pc'";
            mysqli_query($conn, $update_pc_logo);
            $update_mobile_logo = "UPDATE `landing_logo` SET `img_name`= '$logoImgName' WHERE `device` = 'mobile'";
            mysqli_query($conn, $update_mobile_logo);
        }

        $isImage = true;
    }

    if ($isImage) {
        $_SESSION["logo_upload"] = true;
        header("Location: ../views/home.php");
    } else {
        $_SESSION["logo_upload"] = false;
        header("Location: ../views/home.php");
    }
} else {
    header("Location: ../views/login.php");
}
