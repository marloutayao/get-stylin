<?php
session_start();
include("connection.php");
if (isset($_SESSION["user"])) {

    $intro_header = mysqli_real_escape_string($conn, $_POST["intro_header"]);
    $intro_message = mysqli_real_escape_string($conn, $_POST["intro_message"]);
    $isSuccess = false;

    if ($intro_header == "" || $intro_message == "") {
        $isSuccess = false;
    } else {
        $update_intro = "UPDATE `landing_intro` SET `intro_header`= '$intro_header', `intro_message` = '$intro_message' WHERE `id` = 1";
        mysqli_query($conn, $update_intro);
        $isSuccess = true;
    }


    if ($isSuccess) {
        $_SESSION["intro_upload"] = true;
        header("Location: ../views/intro.php");
    } else {
        $_SESSION["intro_upload"] = false;
        header("Location: ../views/intro.php");
    }
} else {
    header("Location: ../views/login.php");
}
