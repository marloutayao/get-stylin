<?php
include("./partials/header.php");
function getTitle()
{
    return "Feature Page";
}

$get_feature = "SELECT * FROM `landing_feature`";
$result = mysqli_query($conn, $get_feature);
$feature_details = mysqli_fetch_assoc($result);

$get_first_picture = "SELECT * FROM `landing_feature_image` WHERE `id` = 1";
$result = mysqli_query($conn, $get_first_picture);
$first_picture = mysqli_fetch_assoc($result);

$get_second_picture = "SELECT * FROM `landing_feature_image` WHERE `id` = 2";
$result = mysqli_query($conn, $get_second_picture);
$second_picture = mysqli_fetch_assoc($result);

$get_third_picture = "SELECT * FROM `landing_feature_image` WHERE `id` = 3";
$result = mysqli_query($conn, $get_third_picture);
$third_picture = mysqli_fetch_assoc($result);

$get_fourth_picture = "SELECT * FROM `landing_feature_image` WHERE `id` = 4";
$result = mysqli_query($conn, $get_fourth_picture);
$fourth_picture = mysqli_fetch_assoc($result);



?>

<div class="container mt-3">
    <div class="jumbotron">
        <?php
        if (isset($_SESSION["feature_updated"])) {
            if ($_SESSION["feature_updated"]) {
                echo "
                    <div class='alert alert-success text-center' role='alert'>
                        Feature updated!
                    </div>
                    ";
                unset($_SESSION["feature_updated"]);
            } else {
                echo "
                    <div class='alert alert-danger text-center' role='alert'>
                        Ooops, there is an error. please check your inputs!
                    </div>
                    ";
                unset($_SESSION["feature_updated"]);
            }
        }

        ?>

        <?php
        if (isset($_SESSION["first_image_updated"])) {
            if ($_SESSION["first_image_updated"]) {
                echo "
                    <div class='alert alert-success text-center' role='alert'>
                        First Image updated!
                    </div>
                    ";
                unset($_SESSION["first_image_updated"]);
            } else {
                echo "
                    <div class='alert alert-danger text-center' role='alert'>
                        Ooops, there is an error. please check your file input!
                    </div>
                    ";
                unset($_SESSION["first_image_updated"]);
            }
        }

        ?>

        <?php
        if (isset($_SESSION["second_image_updated"])) {
            if ($_SESSION["second_image_updated"]) {
                echo "
                    <div class='alert alert-success text-center' role='alert'>
                        First Image updated!
                    </div>
                    ";
                unset($_SESSION["second_image_updated"]);
            } else {
                echo "
                    <div class='alert alert-danger text-center' role='alert'>
                        Ooops, there is an error. please check your file input!
                    </div>
                    ";
                unset($_SESSION["second_image_updated"]);
            }
        }

        ?>

        <?php
        if (isset($_SESSION["third_image_updated"])) {
            if ($_SESSION["third_image_updated"]) {
                echo "
                    <div class='alert alert-success text-center' role='alert'>
                        First Image updated!
                    </div>
                    ";
                unset($_SESSION["third_image_updated"]);
            } else {
                echo "
                    <div class='alert alert-danger text-center' role='alert'>
                        Ooops, there is an error. please check your file input!
                    </div>
                    ";
                unset($_SESSION["third_image_updated"]);
            }
        }

        ?>

        <?php
        if (isset($_SESSION["fourth_image_updated"])) {
            if ($_SESSION["fourth_image_updated"]) {
                echo "
                    <div class='alert alert-success text-center' role='alert'>
                        First Image updated!
                    </div>
                    ";
                unset($_SESSION["fourth_image_updated"]);
            } else {
                echo "
                    <div class='alert alert-danger text-center' role='alert'>
                        Ooops, there is an error. please check your file input!
                    </div>
                    ";
                unset($_SESSION["fourth_image_updated"]);
            }
        }

        ?>
        <h2 class="text-center">Manage Your Feature</h2>
        <hr>
        <div class="row">
            <div class="col-md-12 mx-auto">
                <nav>
                    <div class="nav nav-tabs" id="nav-tab" role="tablist">
                        <a class="nav-item nav-link active" id="header-message-tab" data-toggle="tab" href="#header-message" role="tab" aria-controls="header-message" aria-selected="true">Header | Message</a>
                        <a class="nav-item nav-link" id="feature-image-tab" data-toggle="tab" href="#feature-image" role="tab" aria-controls="feature-image" aria-selected="false">Images</a>
                    </div>
                </nav>
                <div class="tab-content" id="nav-tabContent">
                    <div class="tab-pane fade show active" id="header-message" role="tabpanel" aria-labelledby="header-message-tab">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-6 mx-auto">
                                    <form method="POST" action="../controllers/featureController.php">
                                        <div class="form-group">
                                            <label for="feature_header">Header</label>
                                            <input type="text" class="form-control" id="feature_header" name="feature_header" value="<?= $feature_details["feature_header"] ?>">
                                        </div>

                                        <div class="form-group">
                                            <label for="feature_message">Message</label>
                                            <textarea class="form-control" id="feature_message" rows="5" name="feature_message"><?= $feature_details["feature_message"] ?></textarea>
                                        </div>
                                        <button class="btn btn-outline-dark text-center d-block mx-auto btn-lg" value="<?= $feature_details["id"] ?>" name="feature_id"> Save </button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="feature-image" role="tabpanel" aria-labelledby="feature-image-tab">
                        <nav>
                            <div class="nav nav-tabs mx-auto" id="nav-tab" role="tablist">
                                <a class="nav-item nav-link active" id="first-picture-tab" data-toggle="tab" href="#first-picture" role="tab" aria-controls="first-picture" aria-selected="true">First Image</a>
                                <a class="nav-item nav-link" id="second-picture-tab" data-toggle="tab" href="#second-picture" role="tab" aria-controls="second-picture" aria-selected="false">Second Image</a>
                                <a class="nav-item nav-link" id="third-picture-tab" data-toggle="tab" href="#third-picture" role="tab" aria-controls="third-picture" aria-selected="false">Third Image</a>
                                <a class="nav-item nav-link" id="fourth-picture-tab" data-toggle="tab" href="#fourth-picture" role="tab" aria-controls="fourth-picture" aria-selected="false">Fourth Image</a>
                            </div>
                        </nav>
                        <div class="tab-content" id="nav-tabContent">
                            <div class="tab-pane fade show active" id="first-picture" role="tabpanel" aria-labelledby="first-picture-tab">
                                <div class="row">
                                    <div class="col-md-6">
                                        <img class="img-fluid img-feature-height" src="../assets/<?= $first_picture["img_name"]  ?>" alt="Get Stylin Logo">
                                    </div>
                                    <div class="col-md-6">
                                        <h2 class="text-center">Change Image</h2>
                                        <form action="../controllers/change_first_picture.php" method="POST" enctype="multipart/form-data">

                                            <hr>
                                            <div class="form-group">
                                                <label for="feature-logo">Select a picture</label>
                                                <input type="file" class="form-control-file" name="img" id="feature-logo">
                                                <small><b>Note: .jpg - .png - .jpeg only [ 8mb max file size ]</b></small>
                                            </div>

                                            <button class="btn btn-outline-dark mx-auto" value="<?= $first_picture["id"] ?>" name="img_id">Update Image</button>

                                        </form>
                                    </div>
                                </div>

                            </div>
                            <div class="tab-pane fade show" id="second-picture" role="tabpanel" aria-labelledby="second-picture-tab">
                                <div class="row">
                                    <div class="col-md-6">
                                        <img class="img-fluid img-feature-height" src="../assets/<?= $second_picture["img_name"]  ?>" alt="Get Stylin Logo">
                                    </div>
                                    <div class="col-md-6">
                                        <h2 class="text-center">Change Image</h2>
                                        <form action="../controllers/change_second_picture.php" method="POST" enctype="multipart/form-data">

                                            <hr>
                                            <div class="form-group">
                                                <label for="feature-logo">Select a picture</label>
                                                <input type="file" class="form-control-file" name="img" id="feature-logo">
                                                <small><b>Note: .jpg - .png - .jpeg only [ 8mb max file size ]</b></small>
                                            </div>

                                            <button class="btn btn-outline-dark mx-auto" value="<?= $second_picture["id"] ?>" name="img_id">Update Image</button>

                                        </form>
                                    </div>
                                </div>

                            </div>
                            <div class="tab-pane fade show" id="third-picture" role="tabpanel" aria-labelledby="third-picture-tab">
                                <div class="row">
                                    <div class="col-md-6">
                                        <img class="img-fluid img-feature-height" src="../assets/<?= $third_picture["img_name"]  ?>" alt="Get Stylin Logo">
                                    </div>
                                    <div class="col-md-6">
                                        <h2 class="text-center">Change Image</h2>
                                        <form action="../controllers/change_third_picture.php" method="POST" enctype="multipart/form-data">

                                            <hr>
                                            <div class="form-group">
                                                <label for="feature-logo">Select a picture</label>
                                                <input type="file" class="form-control-file" name="img" id="feature-logo">
                                                <small><b>Note: .jpg - .png - .jpeg only [ 8mb max file size ]</b></small>
                                            </div>

                                            <button class="btn btn-outline-dark mx-auto" value="<?= $third_picture["id"] ?>" name="img_id">Update Image</button>

                                        </form>
                                    </div>
                                </div>

                            </div>
                            <div class="tab-pane fade" id="fourth-picture" role="tabpanel" aria-labelledby="fourth-picture-tab">
                                <div class="row">
                                    <div class="col-md-6">
                                        <img class="img-fluid img-feature-height" src="../assets/<?= $fourth_picture["img_name"]  ?>" alt="Get Stylin Logo">
                                    </div>
                                    <div class="col-md-6">
                                        <h2 class="text-center">Change Image</h2>
                                        <form action="../controllers/change_fourth_picture.php" method="POST" enctype="multipart/form-data">

                                            <hr>
                                            <div class="form-group">
                                                <label for="feature-logo">Select a picture</label>
                                                <input type="file" class="form-control-file" name="img" id="feature-logo">
                                                <small><b>Note: .jpg - .png - .jpeg only [ 8mb max file size ]</b></small>
                                            </div>

                                            <button class="btn btn-outline-dark mx-auto" value="<?= $fourth_picture["id"] ?>" name="img_id">Update Image</button>

                                        </form>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include("./partials/footer.php"); ?>