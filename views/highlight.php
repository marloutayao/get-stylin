<?php
include("./partials/header.php");
function getTitle()
{
    return "Highlights Page";
}
$get_highlights = "SELECT * FROM `landing_highlights`";
$highlights = mysqli_query($conn, $get_highlights);

?>


<div class="container mt-3">
    <div class="jumbotron">
        <?php
        if (isset($_SESSION["highlight_deleted"])) {
            if ($_SESSION["highlight_deleted"]) {
                echo "
                    <div class='alert alert-success text-center' role='alert'>
                        Highlight Deleted!
                    </div>
                    ";
                unset($_SESSION["highlight_deleted"]);
            }
        }

        if (isset($_SESSION["highlight_added"])) {
            if ($_SESSION["highlight_added"]) {
                echo "
                    <div class='alert alert-success text-center' role='alert'>
                        Highlight Added!
                    </div>
                    ";
                unset($_SESSION["highlight_added"]);
            } else {
                echo "
                    <div class='alert alert-danger text-center' role='alert'>
                        Ooops, there is an error please check your inputs!
                    </div>
                    ";
                unset($_SESSION["highlight_added"]);
            }
        }

        ?>



        <h2 class="text-center">Manage Your Highlights</h2>
        <hr>
        <div class="row">
            <div class="col-md-6">
                <h2 class="text-center">Current Highlights</h2>
                <form method="POST" action="../controllers/highlightController.php">
                    <input type="hidden" value="remove_highlight" name="highlight_method">
                    <ul class="list-group">
                        <?php
                        foreach ($highlights as $highlight) {
                        ?>
                            <li class="list-group-item">
                                <?= $highlight["highlight_message"] ?>

                                <button name="highlight_id" class="badge badge-danger badge-pill" value="<?= $highlight["id"] ?>">Remove</button>

                            </li>
                        <?php
                        }
                        ?>
                    </ul>
                </form>
            </div>
            <div class="col-md-6">
                <h2 class="text-center">Add Highlight</h2>
                <form method="POST" action="../controllers/highlightController.php">
                    <input type="hidden" value="add_highlight" name="highlight_method">
                    <div class="form-group">
                        <label for="intro_message">Highlight Message</label>
                        <textarea class="form-control" id="intro_message" rows="4" name="highlight_message"></textarea>
                    </div>
                    <button class="btn btn-outline-dark mx-auto d-block"> Add </button>
                </form>
            </div>
        </div>
    </div>
</div>


<?php include("./partials/footer.php"); ?>