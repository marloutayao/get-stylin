<?php
include("./partials/header.php");
function getTitle()
{
    return "Intro Page";
}

$get_intro = "SELECT * FROM `landing_intro`";
$result = mysqli_query($conn, $get_intro);
$intro_details = mysqli_fetch_assoc($result);

?>

<div class="container mt-3">
    <div class="jumbotron">
        <?php
        if (isset($_SESSION["intro_upload"])) {
            if ($_SESSION["intro_upload"]) {
                echo "
                    <div class='alert alert-success text-center' role='alert'>
                        Intro Updated!
                    </div>
                    ";
                unset($_SESSION["intro_upload"]);
            } else {
                echo "
                    <div class='alert alert-danger text-center' role='alert'>
                        Intro is not updated, please check the inputs!
                    </div>
                    ";
                unset($_SESSION["intro_upload"]);
            }
        }

        ?>
        <h2 class="text-center">Manage Your Intro</h2>
        <hr>
        <div class="row">
            <div class="col-md-6 mx-auto">
                <form method="POST" action="../controllers/introController.php">
                    <div class="form-group">
                        <label for="intro_header">Header</label>
                        <input type="text" class="form-control" id="intro_header" name="intro_header" value="<?= $intro_details["intro_header"] ?>">
                    </div>

                    <div class="form-group">
                        <label for="intro_message">Message</label>
                        <textarea class="form-control" id="intro_message" rows="5" name="intro_message"><?= $intro_details["intro_message"] ?></textarea>
                    </div>
                    <button class="btn btn-outline-dark text-center d-block mx-auto btn-lg"> Save </button>
                </form>

            </div>
        </div>
    </div>
</div>

<?php include("./partials/footer.php"); ?>