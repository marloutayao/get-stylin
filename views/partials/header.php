<?php
session_start();
include("../controllers/connection.php");
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta name="viewport" content="width=device-width,initial-scale=1.0, maximum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="UTF-8">
    <title><?php echo getTitle(); ?> | Get Stylin </title>

    <!-- font awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

    <link rel="stylesheet" href="../css/style.css">

</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-nav-bar" aria-controls="main-nav-bar" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <ul class="navbar-nav">
            <li class="nav-brand active">
                <a class="nav-link" href="/views/login.php">
                    <img class="img-fluid" src="../assets/getstylin-logo.png" alt="Get Stylin Logo">
                </a>
            </li>
        </ul>
        <div class="collapse navbar-collapse justify-content-md-center" id="main-nav-bar">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="./home.php">Logo</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="./intro.php">Intro</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="./highlight.php">Highlights</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="./feature.php">Features</a>
                </li>
                <!-- <li class="nav-item">
                    <a class="nav-link" href="#">Contact US</a>
                </li> -->
                <li class="nav-item">
                    <a class="nav-link" href="./landing_footer.php">Footer</a>
                </li>

                <?php
                if (isset($_SESSION["user"])) {
                    echo '
                        <li class="nav-item">
                            <a class="nav-link" href="./logout.php">Log Out</a>
                         </li>
                        ';
                } else {
                    echo '
                        <li class="nav-item">
                            <a class="nav-link" href="./login.php">Log In</a>
                         </li>
                        ';
                }
                ?>

            </ul>
        </div>
    </nav>