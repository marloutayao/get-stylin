<?php include("./partials/header.php");
function getTitle()
{
    return "Login Page";
}

// echo $_SESSION["user"]["username"];
if (isset($_SESSION["user"])) {
    header("Location: ../views/home.php");
}
?>

<div class="container">
    <div class="jumbotron my-4 login-jumbotron">
        <div class="row shadow-lg login-row">
            <div class="col-md-4 my-5">
                <h2 class="text-center my-5">Log In</h2>
                <form action="../controllers/authenticate.php" method="POST">
                    <div class="form-group">
                        <label for="username">Username</label>
                        <input type="text" class="form-control" id="username" name="username">
                    </div>
                    <div class="form-group">
                        <label for="password">Password</label>
                        <input type="password" class="form-control" id="password" name="password">
                    </div>
                    <button type="submit" class="btn btn-outline-dark my-4"> Login <i class="fas fa-long-arrow-alt-right"></i></button>
                </form>
            </div>
            <div class="col-md-8">
                <img class="img-fluid img-fluid-login" src="../assets/gs-logo-2.png" alt="Get Stylin Logo">
            </div>
        </div>
    </div>
</div>







<?php include("./partials/footer.php"); ?>