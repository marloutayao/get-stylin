<?php
include("./partials/header.php");
function getTitle()
{
    return "Footer Message Page";
}

$get_footer_message = "SELECT * FROM `landing_footer`";
$result = mysqli_query($conn, $get_footer_message);
$landing_footer_details = mysqli_fetch_assoc($result);

?>

<div class="container mt-3">
    <div class="jumbotron">
        <?php
        if (isset($_SESSION["footer_updated"])) {
            if ($_SESSION["footer_updated"]) {
                echo "
                    <div class='alert alert-success text-center' role='alert'>
                        Footer Message Updated!
                    </div>
                    ";
                unset($_SESSION["footer_updated"]);
            } else {
                echo "
                    <div class='alert alert-danger text-center' role='alert'>
                    Footer Message is not updated, please check the inputs!
                    </div>
                    ";
                unset($_SESSION["footer_updated"]);
            }
        }

        ?>
        <h2 class="text-center">Manage Your Footer Message</h2>
        <hr>
        <div class="row">
            <div class="col-md-6 mx-auto">
                <form method="POST" action="../controllers/landingFooterController.php">

                    <div class="form-group">
                        <label for="landing_footer_message">Message</label>
                        <textarea class="form-control" id="landing_footer_message" rows="5" name="landing_footer_message"><?= $landing_footer_details["footer_message"] ?></textarea>
                    </div>
                    <button class="btn btn-outline-dark text-center d-block mx-auto btn-lg"> Save </button>
                </form>

            </div>
        </div>
    </div>
</div>

<?php include("./partials/footer.php"); ?>