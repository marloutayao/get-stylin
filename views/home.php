<?php
include("./partials/header.php");
function getTitle()
{
    return "Home Page";
}

$get_pc_logo = "SELECT `id`, `img_name`, `device` FROM `landing_logo` WHERE `device` = 'pc'";
$result = mysqli_query($conn, $get_pc_logo);
$pc_logo = mysqli_fetch_assoc($result);

$get_mobile_logo = "SELECT `id`, `img_name`, `device` FROM `landing_logo` WHERE `device` = 'mobile'";
$result = mysqli_query($conn, $get_mobile_logo);
$mobile_logo = mysqli_fetch_assoc($result);

?>

<div class="container mt-3">
    <div class="jumbotron">

        <?php
        if (isset($_SESSION["logo_upload"])) {
            if ($_SESSION["logo_upload"]) {
                echo "
                    <div class='alert alert-success text-center' role='alert'>
                        Logo Uploaded!
                    </div>
                    ";
                unset($_SESSION["logo_upload"]);
            } else {
                echo "
                    <div class='alert alert-danger text-center' role='alert'>
                        Logo is not uploaded, please check the uploaded file
                    </div>
                    ";
                unset($_SESSION["logo_upload"]);
            }
        }

        ?>
        <h2 class="text-center">Manage Your Logo</h2>
        <hr>
        <div class="row">
            <div class="col-md-6">
                <h2 class="text-center">Current Logo</h2>
                <nav>
                    <div class="nav nav-tabs" id="nav-tab" role="tablist">
                        <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">PC</a>
                        <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Mobile</a>
                    </div>
                </nav>
                <div class="tab-content" id="nav-tabContent">
                    <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                        <img class="img-fluid" src="../assets/<?= $pc_logo["img_name"]  ?>" alt="Get Stylin Logo">
                    </div>
                    <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                        <img class="img-fluid" src="../assets/<?= $mobile_logo["img_name"]  ?>" alt="Get Stylin Logo">
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <h2 class="text-center">Change your Logo</h2>
                <form action="../controllers/logoController.php" method="POST" enctype="multipart/form-data">
                    <div class="btn-group btn-group-toggle" data-toggle="buttons">
                        <label class="btn btn-dark active">
                            <input type="radio" name="change_options" id="pc-only" value="pc_only" checked> For PC only
                        </label>
                        <label class="btn btn-dark">
                            <input type="radio" name="change_options" id="mobile-only" value="mobile_only"> For Mobile only
                        </label>
                        <label class="btn btn-dark">
                            <input type="radio" name="change_options" id="change-both" value="change_both"> Change Both
                        </label>
                    </div>
                    <hr>
                    <div class="form-group">
                        <label for="landing-logo">Select a picture</label>
                        <input type="file" class="form-control-file" name="logoImg" id="landing-logo">
                        <small><b>Note: .jpg - .png - .jpeg only [ 8mb max file size ]</b></small>
                    </div>

                    <button class="btn btn-outline-dark mx-auto">Upload Image</button>

                </form>
            </div>
        </div>
    </div>
</div>

<?php include("./partials/footer.php"); ?>